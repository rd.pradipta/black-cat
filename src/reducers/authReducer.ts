import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "../actions/types";

const loggedInUser = JSON.parse(localStorage.getItem("userToken") || "{}");
const initialState =
  loggedInUser !== "{}"
    ? { isLoggedIn: true, loggedInUser }
    : { isLoggedIn: false, loggedInUser: null };

export default function authReducer(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: payload.user,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
      };
    default:
      return state;
  }
}
