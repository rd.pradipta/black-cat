// Request Header for every HTTP request being sent
import axios from "axios";

const BACKEND_URL = process.env.REACT_APP_API;

const requestHeader = axios.create({
  baseURL: BACKEND_URL,
});

export default requestHeader;
