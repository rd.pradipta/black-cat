import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, SET_MESSAGE } from "./types";
import authService from "../services/auth.service";

export const login = (username: string, password: string) => {
  return (dispatch: any) => {
    const payload = { username, password };
    return authService.login(payload).then(
      (data: string) => {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: { user: data },
        });
        return Promise.resolve();
      },
      (error: any) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        dispatch({
          type: LOGIN_FAIL,
        });
        dispatch({
          type: SET_MESSAGE,
          payload: message,
        });
        return Promise.reject();
      }
    );
  };
};

export const logout = () => {
  return (dispatch: any) => {
    authService.logout();
    dispatch({
      type: LOGOUT,
    });
  };
};
