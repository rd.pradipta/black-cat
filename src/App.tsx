import "./App.scss";

// React Router
import { BrowserRouter, Routes, Route } from "react-router-dom";

// Redux store
import { Provider } from "react-redux";
import configureStore from "./store";

// Pages
import FeedPage from "./pages/feed/Feed";
import LoginPage from "./pages/login/Login";
import NotFoundPage from "./pages/not-found/NotFound";

const App = () => {
  return (
    <Provider store={configureStore()}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<FeedPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
