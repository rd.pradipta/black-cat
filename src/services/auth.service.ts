import requestHeader from "../helpers/requestHeader";

interface LoginInterface {
  username: string;
  password: string;
}

export default {
  login(payload: LoginInterface) {
    return requestHeader
      .get(`/tokens/?username=${payload.username}`)
      .then(response => {
        const userData = response.data[0];
        if (userData.length !== 0)
          localStorage.setItem("userToken", userData.token);
        return userData.token;
      });
  },
  logout() {
    localStorage.removeItem("userToken");
  },
};
